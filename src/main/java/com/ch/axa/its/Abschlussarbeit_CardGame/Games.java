package com.ch.axa.its.Abschlussarbeit_CardGame;

public enum Games {

	  THEKINGISDEAD ("The king is dead"),
	  SOLITAIRE ("Solitaire");

	  private String value;

	  Games(String value) {
	    this.value = value;
	  }

	  public String toString() {
	    return value;
	  }}
