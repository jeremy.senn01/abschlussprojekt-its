package com.ch.axa.its.Abschlussarbeit_CardGame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.image.ImageView;
/**
 * 
 * @author Jeremy Senn
 *
 */
public class SolitaireGameService {

	private DeckOfCardsProxy proxy;
	private Card card;
	private Deck deck;
	private Map<Integer, ArrayList<Card>> stacks;
	private List<Card> beginStackCards;
	
	public SolitaireGameService() {
		proxy = new DeckOfCardsProxy();

		// Initialize all the Stacks
		stacks = new HashMap<>();

		deck = proxy.getNewDeck();
		List<Card> allCards = proxy.drawAllCardsFromDeck().getCards();

		for (int i = 1; i < 14; i++) {
			stacks.put(i, new ArrayList<>());
		}

		stacks.get(1).add(allCards.get(0));

		stacks.get(2).add(allCards.get(1));
		stacks.get(2).add(allCards.get(2));

		stacks.get(3).add(allCards.get(3));
		stacks.get(3).add(allCards.get(4));
		stacks.get(3).add(allCards.get(5));

		stacks.get(4).add(allCards.get(6));
		stacks.get(4).add(allCards.get(7));
		stacks.get(4).add(allCards.get(8));
		stacks.get(4).add(allCards.get(9));

		stacks.get(5).add(allCards.get(10));
		stacks.get(5).add(allCards.get(11));
		stacks.get(5).add(allCards.get(12));
		stacks.get(5).add(allCards.get(13));
		stacks.get(5).add(allCards.get(14));

		stacks.get(6).add(allCards.get(15));
		stacks.get(6).add(allCards.get(16));
		stacks.get(6).add(allCards.get(17));
		stacks.get(6).add(allCards.get(18));
		stacks.get(6).add(allCards.get(19));
		stacks.get(6).add(allCards.get(20));

		stacks.get(7).add(allCards.get(21));
		stacks.get(7).add(allCards.get(22));
		stacks.get(7).add(allCards.get(23));
		stacks.get(7).add(allCards.get(24));
		stacks.get(7).add(allCards.get(25));
		stacks.get(7).add(allCards.get(26));
		stacks.get(7).add(allCards.get(27));

		beginStackCards = new ArrayList<>();
		int cardIndex = 0;
		for (Card card : allCards) {
			if (cardIndex > 27) {
				beginStackCards.add(card);
			}
			cardIndex++;
		}
	}

	public Deck getNewDeck() {
		return proxy.getNewDeck();
	}

	public Card getCardFromDeck() {
		card = proxy.drawCardFromDeck();
		return card;
	}

	public List<Card> getAllCardsFromDeck() {
		List<Card> allCards = new ArrayList<>();
		Deck deck = proxy.drawAllCardsFromDeck();
		allCards = deck.getCards();
		return allCards;

	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public Deck getDeck() {
		return deck;
	}

	public void setDeck(Deck deck) {
		this.deck = deck;
	}

	public Map<Integer, ArrayList<Card>> getStacks() {
		return stacks;
	}

	public void setStacks(Map<Integer, ArrayList<Card>> stacks) {
		this.stacks = stacks;
	}

	public List<Card> getBeginStackCards() {
		return beginStackCards;
	}

	public void setBeginStackCards(List<Card> beginStackCards) {
		this.beginStackCards = beginStackCards;
	}
}
