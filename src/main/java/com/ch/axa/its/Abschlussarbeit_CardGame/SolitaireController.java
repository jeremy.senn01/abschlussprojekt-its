package com.ch.axa.its.Abschlussarbeit_CardGame;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

/**
 * 
 * @author Jeremy Senn
 *
 */
public class SolitaireController implements Initializable, Controller {

	@FXML
	private Pane root;
	@FXML
	private Button bBack;
	@FXML
	private ImageView ivDeck;
	@FXML
	private ImageView ivFlippedCard;
	@FXML
	private ImageView ivFinalStackSpades;
	@FXML
	private ImageView ivFinalStackHearts;
	@FXML
	private ImageView ivFinalStackClubs;
	@FXML
	private ImageView ivFinalStackDiamonds;
	@FXML
	private ImageView ivRow7;
	@FXML
	private ImageView ivRow6;
	@FXML
	private ImageView ivRow5;
	@FXML
	private ImageView ivRow4;
	@FXML
	private ImageView ivRow3;
	@FXML
	private ImageView ivRow2;
	@FXML
	private ImageView ivRow1;
	@FXML
	private Text tScore;
	@FXML
	private Text tTime;

	private SolitaireGameService service;
	private URLConnection conn;

	private List<Card> beginStackCards;
	private List<Card> flippedCards;

	private Map<Integer, ArrayList<Card>> stacks;
	private Map<Integer, HashMap<Integer, ImageView>> imageViews;
	private Map<Image, Card> cardsMap;

	private Timer timer;

	private final Image EMPTY_CARD = new Image("/Images/card_empty.jpg");
	private final Image BACKSIDE_CARD = new Image("/Images/card_backside.jpg");
	private final int Y_OFFSET = 15;

	private int countClicks = 0;

	private Card firstCard;
	private Card secondCard;
	private ImageView firstIv;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		beginStackCards = new ArrayList<>();
		flippedCards = new ArrayList<>();
		cardsMap = new HashMap<>();
		timer = new Timer();

		// Create a service
		service = new SolitaireGameService();

		// Set deck image
		ivDeck.setImage(BACKSIDE_CARD);

		ivFinalStackClubs.setImage(EMPTY_CARD);
		ivFinalStackDiamonds.setImage(EMPTY_CARD);
		ivFinalStackHearts.setImage(EMPTY_CARD);
		ivFinalStackSpades.setImage(EMPTY_CARD);

		// Put ImageViews into HashMap
		imageViews = new HashMap<>();

		// Initialize HashMap for 1. "Column"
		imageViews.put(1, new HashMap<Integer, ImageView>());
		// put an the imageView at 1. Place in "Column"
		imageViews.get(1).put(0, ivRow1);

		imageViews.put(2, new HashMap<Integer, ImageView>());
		imageViews.get(2).put(0, ivRow2);

		imageViews.put(3, new HashMap<Integer, ImageView>());
		imageViews.get(3).put(0, ivRow3);

		imageViews.put(4, new HashMap<Integer, ImageView>());
		imageViews.get(4).put(0, ivRow4);

		imageViews.put(5, new HashMap<Integer, ImageView>());
		imageViews.get(5).put(0, ivRow5);

		imageViews.put(6, new HashMap<Integer, ImageView>());
		imageViews.get(6).put(0, ivRow6);

		imageViews.put(7, new HashMap<Integer, ImageView>());
		imageViews.get(7).put(0, ivRow7);

		imageViews.put(8, new HashMap<Integer, ImageView>());
		imageViews.get(8).put(0, ivDeck);

		imageViews.put(9, new HashMap<Integer, ImageView>());
		imageViews.get(9).put(0, ivFlippedCard);

		imageViews.put(10, new HashMap<Integer, ImageView>());
		imageViews.get(10).put(0, ivFinalStackSpades);

		imageViews.put(11, new HashMap<Integer, ImageView>());
		imageViews.get(11).put(0, ivFinalStackHearts);

		imageViews.put(12, new HashMap<Integer, ImageView>());
		imageViews.get(12).put(0, ivFinalStackClubs);

		imageViews.put(13, new HashMap<Integer, ImageView>());
		imageViews.get(13).put(0, ivFinalStackDiamonds);

		for (HashMap<Integer, ImageView> hm : imageViews.values()) {
			for (ImageView iv : hm.values()) {
				iv.setOnMouseClicked(e -> getClickedCard(e));
			}
		}

		stacks = service.getStacks();

		loadStacks();
		// This List holds all cards that can be flipped
		beginStackCards = service.getBeginStackCards();

		// Button Action
		bBack.setOnAction(e -> this.changeScene());

		// Click action
		ivDeck.setOnMouseClicked(e -> flipCardFromBeginStack(e));

		startTimer();

	}

	public void startTimer() {

		timer.scheduleAtFixedRate(new TimerTask() {

			int seconds = 0;
			int minutes = 0;

			@Override
			public void run() {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (seconds < 10) {
							tTime.setText(minutes + ":0" + seconds);
						} else {
							tTime.setText(minutes + ":" + seconds);
						}
					}
				});
				seconds++;
				if (seconds == 60) {
					minutes++;
					seconds = 0;
				}
			}
		}, 0, 1000);
	}

	public void getClickedCard(final MouseEvent event) {
		int index = -1;
		int column = -1;
		ImageView clickedIv = (ImageView) event.getSource();

		for (Entry<Integer, HashMap<Integer, ImageView>> hmEntry : imageViews.entrySet()) {
			for (Entry<Integer, ImageView> ivEntry : hmEntry.getValue().entrySet()) {
				if (clickedIv.equals(ivEntry.getValue())) {
					index = ivEntry.getKey();
					column = hmEntry.getKey();
				}
			}
		}
		// First click (source)
		if (countClicks % 2 == 0) {
			firstCard = cardsMap.get(clickedIv.getImage());
			firstIv = clickedIv;

		}
		// Second Click (destination)
		else {
			ImageView newIv;
			Image newImage;

			// Bottom Stacks
			if (column < 8) {
				secondCard = cardsMap.get(clickedIv.getImage());

				System.out.println(firstCard);
				System.out.println(secondCard);
				if (!firstCard.getSuit().equals(secondCard.getSuit())) {

					newIv = addImageView(column, index + 1);
					newImage = setImage(firstCard.getImage());
					newIv.setImage(newImage);
					root.getChildren().remove(firstIv);
				}

			}
			// Top-Right Stacks
			else if (column > 9) {
				if (stacks.get(column).size() != 0) {
					
					if (firstCard.getSuit().equals(secondCard.getSuit())) {
						
						clickedIv.setImage(firstIv.getImage());
						stacks.get(column).add(cardsMap.get(clickedIv.getImage()));
						imageViews.get(column).put(0, clickedIv);
						root.getChildren().remove(firstIv);
					}
				} else {
					clickedIv.setImage(firstIv.getImage());
					stacks.get(column).add(cardsMap.get(clickedIv.getImage()));
					imageViews.get(column).put(0, clickedIv);
					root.getChildren().remove(firstIv);
				}
			}
		}
		countClicks++;
	}

	public ImageView addImageView(int column, int index) {

		ImageView newIv = new ImageView();
		imageViews.get(column).put(index, newIv);

		double x = imageViews.get(column).get(index - 1).getLayoutX();
		double y = imageViews.get(column).get(index - 1).getLayoutY();

		if (column < 8) {
			newIv.relocate(x, y + Y_OFFSET);
		} else {
			newIv.relocate(x, y);
		}
		newIv.setFitHeight(imageViews.get(1).get(0).getFitHeight());
		newIv.setFitWidth(imageViews.get(1).get(0).getFitWidth());

		newIv.setOnMouseClicked(e -> getClickedCard(e));

		root.getChildren().add(newIv);

		return newIv;
	}

	public void flipCardFromBeginStack(final MouseEvent event) {
		Card flippedCard = null;

		if (ivDeck.getImage().equals(EMPTY_CARD)) {
			ivDeck.setImage(BACKSIDE_CARD);
		}

		if (beginStackCards.size() > 0) {
			flippedCard = beginStackCards.get(beginStackCards.size() - 1);
			Image fcImage = setImage(flippedCard.getImage());
			ivFlippedCard.setImage(fcImage);
		} else {
			beginStackCards.addAll(flippedCards);
			flippedCards.clear();
			flippedCard = beginStackCards.get(beginStackCards.size() - 1);
			ivDeck.setImage(EMPTY_CARD);
		}
		beginStackCards.remove(flippedCard);
		flippedCards.add(flippedCard);
	}

	public void loadStacks() {

		// Iterate over each ImageView
		for (Entry<Integer, HashMap<Integer, ImageView>> hmEntry : imageViews.entrySet()) {

			for (Entry<Integer, ImageView> ivEntry : hmEntry.getValue().entrySet()) {

				int ivNumber = hmEntry.getKey();

				int column = hmEntry.getKey();
				int indexInColumn = ivEntry.getKey();
				// TODO: Increase performance of setting ImageViews
				// Iterate over each stack
				for (Entry<Integer, ArrayList<Card>> stackEntry : stacks.entrySet()) {

					// Stack and ImageView match
					if (stackEntry.getKey() == ivNumber) {

						double xPos = ivEntry.getValue().getLayoutX();
						double yPos = ivEntry.getValue().getLayoutY();

						// Iterate over each Card in Stack
						for (Card card : stackEntry.getValue()) {

							ImageView newIv = new ImageView();

							newIv.setFitHeight(ivEntry.getValue().getFitHeight());
							newIv.setFitWidth(ivEntry.getValue().getFitWidth());

							// Map Card to Image
							Image cardImage = setImage(card.getImage());
							cardsMap.put(cardImage, card);

							// Map ImageView to Place in Column
							imageViews.get(column).put(indexInColumn, newIv);
							indexInColumn++;

							// The card is the top Card on the Stack -> Show front Image
							if ((stackEntry.getValue().get(stackEntry.getValue().size() - 1)).equals(card)) {
								newIv.setImage(cardImage);
							}
							// Show back image
							else {
								newIv.setImage(BACKSIDE_CARD);
							}
							newIv.relocate(xPos, yPos);

							root.getChildren().add(newIv);

							newIv.setOnMouseClicked(e -> getClickedCard(e));

							yPos += Y_OFFSET;
						}
					}
				}
			}
		}
	}

	public Image setImage(String image) {

		try {
			// Establish Connection and get Picture
			conn = new URL(image).openConnection();
			conn.setRequestProperty("User-Agent", "Chrome/69.0.3497.100");
			InputStream stream = conn.getInputStream();
			Image cardImage = new Image(stream);
			return cardImage;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void changeScene() {
		timer.cancel();
		GameLoader gLoader = new GameLoader();
		gLoader.setScene(new MainController(), "/fxml/MainMenu.fxml");
	}
}