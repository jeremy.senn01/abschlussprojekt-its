package com.ch.axa.its.Abschlussarbeit_CardGame;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class MainApp extends Application {
	
	private Parent root;
	
    @Override
    public void start(Stage stage) throws Exception {
    	URL url = this.getClass().getResource("/fxml/MainMenu.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        loader.setController(new MainController());
        
        root = loader.load();
        
        Scene scene = new Scene(root);
        
        stage.setTitle("JavaFX and Maven");
        stage.setScene(scene);
        stage.show();
    }
    
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);

    }

}
