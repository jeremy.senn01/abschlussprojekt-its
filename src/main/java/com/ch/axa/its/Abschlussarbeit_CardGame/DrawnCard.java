package com.ch.axa.its.Abschlussarbeit_CardGame;

import java.util.List;

public class DrawnCard {

	private List<Card> cards;
	private boolean success;
	private String deck_id;
	private int remaining;

	public DrawnCard() {
		
	}

	public DrawnCard(List<Card> cards, boolean success, String deck_id, int remaining) {
		this.cards = cards;
		this.success = success;
		this.deck_id = deck_id;
		this.remaining = remaining;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getDeck_id() {
		return deck_id;
	}

	public void setDeck_id(String deck_id) {
		this.deck_id = deck_id;
	}

	public int getRemaining() {
		return remaining;
	}

	public void setRemaining(int remaining) {
		this.remaining = remaining;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}
}
