package com.ch.axa.its.Abschlussarbeit_CardGame;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
/**
 * 
 * @author Jeremy Senn
 *
 */
public class TheKingIsDeadController implements Initializable, Controller {

	@FXML
	private Button bBack;
	@FXML
	private Button bFlipCard;
	@FXML
	private Button bGenerateDeck;
	@FXML
	private ComboBox<String> bChoseCard;
	@FXML
	private ImageView ivDeck;
	@FXML
	private ImageView ivCard;
	@FXML
	private Text tError;
	@FXML
	private Text tAvailableCards;

	private Deck deck;
	private Card card;
	private TheKingIsDeadGameService service;
	private URLConnection conn;
	private ObservableList<String> allCards;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		service = new TheKingIsDeadGameService();

		// Add all values to ComboBox
		
		allCards = FXCollections.observableArrayList();
		for (Entry<String, String> entry : service.loadAvailableCards().entrySet()) {
			allCards.add(entry.getValue());
		}
		bChoseCard.setItems(allCards);

		// Listen for change in ComboBox
		bChoseCard.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
			service.setSelectedCard(newValue);
			System.out.println(newValue);
			if (bFlipCard.isDisabled()) {
				bFlipCard.setDisable(false);
			}
			bChoseCard.setDisable(true);
			allCards.remove(oldValue);
		});

		// Button action
		bBack.setOnAction(e -> this.changeScene());
		bFlipCard.setOnAction(e -> this.flipCard());
		bGenerateDeck.setOnAction(e -> this.generateDeck());

	}

	public void generateDeck() {

		deck = service.getNewDeck();

		if (ivDeck.getImage() == null) {
			ivDeck.setImage(new Image("/Images/card_Backside.jpg"));
		}
		
	}

	public void flipCard() {

		tError.setText("");

		//check for broken rules
		if (deck == null) {
			tError.setText("Please generate a deck first");
		} else if (deck.getRemaining() == 0) {
			tError.setText("No cards left");
		} else if (bChoseCard.getSelectionModel().getSelectedItem() == null) {
			tError.setText("Chose a card first!");
		} else {
			
			//Draw Card and update information
			card = service.drawCardFromDeck();
			setImage(card.getImage());
			tAvailableCards.setText(Integer.toString(deck.getRemaining()) + " Cards");
			
			//When The Card matches, print it
			if (service.hasWinner()) {
				tError.setText("CARD MATCHES! YOU LOST");
				bChoseCard.setDisable(false);
				bFlipCard.setDisable(true);
			}
		}
	}

	public void setImage(String image) {

		try {
			// Establish Connection and get Picture
			conn = new URL(image).openConnection();
			conn.setRequestProperty("User-Agent", "Chrome/69.0.3497.100");
			InputStream stream = conn.getInputStream();
			Image cardImage = new Image(stream);
			ivCard.setImage(cardImage);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void changeScene() {
		GameLoader gLoader = new GameLoader();
		gLoader.setScene(new MainController(), "/fxml/MainMenu.fxml");
	}
}