package com.ch.axa.its.Abschlussarbeit_CardGame;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class GameLoader extends Stage {

	public void setScene(Controller controller, String path) {

		URL url = this.getClass().getResource(path);
		FXMLLoader loader = new FXMLLoader(url);
		loader.setController(controller);

		Pane root = null;
		
		try {
			root = loader.load();
			Scene scene = new Scene(root);
			this.setScene(scene);
			this.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
