package com.ch.axa.its.Abschlussarbeit_CardGame;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.plaf.synth.SynthSpinnerUI;

public class TheKingIsDeadGameService implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2902058594749973404L;


	private List<Card> cards;
	private Map<String, String> availableCards;
	private String selectedCard;
	private String flippedCard;
	private DeckOfCardsProxy proxy;

	public TheKingIsDeadGameService() {
		cards = new ArrayList<>();
		availableCards = new HashMap<>();
		proxy = new DeckOfCardsProxy();
	}

	public Deck getNewDeck() {
		return proxy.getNewDeck();
	}

	public Deck getDeck() {
		return proxy.getDeck();
	}


	public Card drawCardFromDeck() {
		//draw Card
		Card card = proxy.drawCardFromDeck();
		
		//set flipped card Variable
		flippedCard = card.getValue() + " " + card.getSuit();
		
		for (Entry<String, String> entry : availableCards.entrySet()) {
			if (entry.getValue().toLowerCase().equals(flippedCard.toLowerCase())) {
				availableCards.remove(entry.getKey());
				break;
			}
		}
		return card;
	}
	
	public boolean hasWinner() {
		if (selectedCard.toLowerCase().equals(flippedCard.toLowerCase())) {
			return true;
		}
		return false;
		
	}
	
	public Map<String, String> loadAvailableCards() {

		List<String> types = new ArrayList<>();
		List<String> values = new ArrayList<>();

		types = Arrays.asList("King", "Queen", "Jack", "0", "9", "8", "7", "6", "5", "4", "3", "2", "ACE");
		values = Arrays.asList("Clubs", "Spades", "Diamonds", "Hearts");

		for (String type : types) {
			for (String value : values) {
				availableCards.put(type.substring(0, 1) + value.substring(0, 1),
						(type.equals("0") ? "1" + type : type) + " " + value);
			}
		}
		return availableCards;
	}

	public List<Card> getCards() {
		return cards;
	}

	public Map<String, String> getAvailableCards() {
		return availableCards;
	}

	public void setAvailableCards(Map<String, String> availableCards) {
		this.availableCards = availableCards;
	}

	public String getSelectedCard() {
		return selectedCard;
	}

	public void setSelectedCard(String selectedCard) {
		this.selectedCard = selectedCard;
	}

	public String getFlippedCard() {
		return flippedCard;
	}

	public void setFlippedCard(String flippedCard) {
		this.flippedCard = flippedCard;
	}
}
