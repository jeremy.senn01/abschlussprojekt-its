package com.ch.axa.its.Abschlussarbeit_CardGame;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

public class MainController implements Initializable, Controller {

	@FXML
	private ComboBox<String> bChoseGame;
	@FXML
	private Button bPlay;
	private ObservableList<String> games = FXCollections.observableArrayList();

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		games.addAll(Games.SOLITAIRE.toString(), Games.THEKINGISDEAD.toString());
		bChoseGame.setItems(games);

		bPlay.setOnAction(e -> this.changeScene());

	}

	@Override
	public void changeScene() {

		String game = bChoseGame.getSelectionModel().getSelectedItem();
		GameLoader gLoader = new GameLoader();

		if (game.equals(Games.SOLITAIRE.toString())) {
			gLoader.setScene(new SolitaireController(), "/fxml/Solitaire.fxml");
		}
		if (game.equals(Games.THEKINGISDEAD.toString())) {
			gLoader.setScene(new TheKingIsDeadController(), "/fxml/TheKingIsDead.fxml");
		}

	}

}
