package com.ch.axa.its.Abschlussarbeit_CardGame;
import java.io.Serializable;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
/**
 * 
 * @author Jeremy Senn
 *
 */
public class DeckOfCardsProxy implements Serializable {

	private static final long serialVersionUID = 2902058594749973404L;

	private Client client;
	private WebTarget target;
	private Deck deck;
	private Card card;


	public DeckOfCardsProxy() {
		client = ClientBuilder.newClient();
	}

	public Deck getNewDeck() {

		target = client.target("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1");
		deck = target.request(MediaType.APPLICATION_JSON).get(Deck.class);
		return deck;
	}

	public Card drawCardFromDeck() {
		String deck_id = getDeck().getDeck_id();
		
		client = ClientBuilder.newClient();
		target = client.target("https://deckofcardsapi.com/api/deck/" + deck_id + "/draw/?count=1");
		DrawnCard drawnCard = target.request(MediaType.APPLICATION_JSON).get(DrawnCard.class);
		
		card = drawnCard.getCards().get(0);
		deck.setRemaining(drawnCard.getRemaining());
		
		return card;
	}
	
	public Deck drawAllCardsFromDeck() {
		String deck_id = getDeck().getDeck_id();
		
		client = ClientBuilder.newClient();
		target = client.target("https://deckofcardsapi.com/api/deck/" + deck_id + "/draw/?count=52");
		deck = target.request(MediaType.APPLICATION_JSON).get(Deck.class);
		
		deck.setRemaining(0);
		
		return deck;
	}
	
	public Deck getDeck() {
		return deck;
	}

	public void setDeck(Deck deck) {
		this.deck = deck;
	}


}
