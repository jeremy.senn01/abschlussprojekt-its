package com.ch.axa.its.Abschlussarbeit;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.mockito.Mockito;

import com.ch.axa.its.Abschlussarbeit_CardGame.Card;
import com.ch.axa.its.Abschlussarbeit_CardGame.Deck;
import com.ch.axa.its.Abschlussarbeit_CardGame.DeckOfCardsProxy;
import com.ch.axa.its.Abschlussarbeit_CardGame.SolitaireGameService;

public class TestCard1 {

	private SolitaireGameService testee;
	private DeckOfCardsProxy proxy;
	private List<Card> cards;
	private Deck deck;
	
	@Before
	public void setUp() throws Exception {
		deck = new Deck();
		cards = new ArrayList<>();
		
		Card c1 = new Card();
		c1.setCode("D7");
		c1.setValue("7");
		c1.setImage("/Images/card_backside.jpg");
		c1.setImages(null);
		c1.setSuit("Diamonds");
		
		cards.add(c1);
		
		deck.setRemaining(51);
		deck.setCards(cards);
		
		proxy = Mockito.mock(DeckOfCardsProxy.class);
		Mockito.when(proxy.getNewDeck()).thenReturn(deck);
		
		//TODO: Complete Test Case
		
	}
}