package com.ch.axa.its.Abschlussarbeit;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ch.axa.its.Abschlussarbeit_CardGame.DeckOfCardsProxy;
import com.ch.axa.its.Abschlussarbeit_CardGame.TheKingIsDeadGameService;

public class TheKingIsDeadGameServiceTest {

	private TheKingIsDeadGameService testee;
	@Mock
	private DeckOfCardsProxy proxy;
	
	@Before
	public void setUp() throws Exception {
	testee = new TheKingIsDeadGameService();
	MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void test_hasWinner_OK() {
		testee.setSelectedCard("8 Clubs");
		testee.setFlippedCard("8 Clubs");
		
		assertEquals(true, testee.hasWinner());
	}
	
	@Test
	public void test_hasWinner_NOK() {
		testee.setSelectedCard("7 Clubs");
		testee.setFlippedCard("8 Clubs");
		
		assertEquals(false, testee.hasWinner());
	}

}
